#!/usr/bin/python3
# -*- coding: utf-8 -*-

import random as rand

from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
#exceptions
from selenium.common.exceptions import TimeoutException as SeleniumTimeoutException

from baseclass import BaseClass
from model.session import Session



class Scrapy(BaseClass):
    def __init__(self):
        # init baseclass
        super().__init__()

        # init session
        # TODO: stop session if google ads for this ip is none
        self.session = Session()

        # webdriver configuration
        module = self.config['webdriver']['module']
        driver = self.config['webdriver']['driver']
        driver_module = __import__(module, fromlist=driver)
        self.driver = getattr(driver_module,driver)()
        self.driver.maximize_window()
    
    def loginToGmail(self):
        """Go to gmail and log in"""
        session_gmail_user = self.session.get
    
    def execute(self):
        """Execute scrapy testing"""

        #register query
        self.query = self.config['scrapy']['query']
        # get page and lookup for query box
        self.driver.get('https://www.google.com/')
        search_box = self.driver.find_element_by_name('q')
        query_actions = ActionChains(self.driver)
        # wait to display page 
        self.driver.implicitly_wait(rand.randrange(5,10))

        # move to searchbox
        query_actions.move_to_element(search_box)
        query_actions.click(search_box)
        query_actions.pause(rand.randrange(1,2))
        
        # type each letter in the search box
        # for letter in list(self.query):
        #     ActionChains(self.driver).send_keys_to_element(search_box,letter).perform()
        #     ActionChains(self.driver).pause(rand.randrange(0,2)).perform()

        # type the full query in search box
        query_actions.send_keys_to_element(search_box,self.query)
        
        # perfom query actions and submit query box
        query_actions.perform()
        search_box.submit()

        # extract ads if apply
        try:
            ads = WebDriverWait(self.driver,10).until(
                EC.presence_of_all_elements_located(
                    (By.XPATH, "//*[contains(@class,'ad_cclk')]/child::a[2]")
                )
            )
            
            # wait
            self.driver.implicitly_wait(rand.randrange(3,5))

            # register this session
            self.session.registerSession(query=self.query, ads_count=len(ads))

            # get the original window
            original_window = self.driver.current_window_handle

            wait = WebDriverWait(self.driver, 10)

            # go to each ad and click it to open a new tab
            for ad in ads:
                # get the ad url
                url = ad.get_attribute('href')
                # get location of ad
                x = ad.location['x']
                y = ad.location['y']
                # scroll to ad
                self.driver.execute_script("window.scrollTo(%s,%s);" % (x,y))
                self.driver.execute_script("window.scrollBy(0,-120);")
                # go to element
                ActionChains(self.driver).move_to_element(ad).perform()
                # pause a moment
                ActionChains(self.driver).pause(rand.randrange(1,5)).perform()
                # press CONTROL key
                ActionChains(self.driver).key_down(Keys.CONTROL).perform()
                # click to ad
                ActionChains(self.driver).click(ad).perform()
                # release CONTROL key
                ActionChains(self.driver).key_up(Keys.CONTROL).perform()
                # wait until there is a new tab
                wait.until(EC.number_of_windows_to_be(2))
                # get the ad page tab
                for window_handle in self.driver.window_handles:
                    if window_handle != original_window:
                        self.driver.switch_to.window(window_handle)
                        break
                
                # wait a while to check the ad
                ActionChains(self.driver).pause(rand.randrange(5,8)).perform()
                # get the page's url
                realpage = self.driver.current_url
                # close the ad page and go back to google
                self.driver.close()
                self.driver.switch_to.window(original_window)

                                
                self.log.info("click en %s" % (url))
                ActionChains(self.driver).pause(rand.randrange(1,5)).perform()

                self.session.registerSessionAds(page=realpage, ad_url=url)
            
            
        except SeleniumTimeoutException:
            self.registerSession(query=self.query, ads_count=0, error=False)
        except Exception as e:
            self.log.error("Unknow error : %s", e)
            self.registerSession(query=self.query, ads_count=0, error=True)
        finally:
            # wait 
            self.driver.implicitly_wait(rand.randrange(5,10))
            # and close
            self.driver.close()
            # and quit
            self.driver.quit()

if __name__ == "__main__":
    session = Scrapy()
    session.execute()
#!/usr/bin/python3
# -*- coding: utf-8 -*-

from logger import logger as baselogger


from config import Config


class BaseClass:
    
    """docstring for BaseClass."""
    def __init__(self):
        # configure config
        self.config = Config().config

        # configure logger
        logger_module = __import__(self.config['logger']['default'], fromlist="Logger")
        self.log = baselogger.Logger(getattr(logger_module, "Logger")())
        # self.log = baselogger.Logger(nativelogger.Logger())
        

if __name__ == "__main__":
    a = BaseClass()
    a.log.info('testing')
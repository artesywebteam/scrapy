#!/usr/bin/python3
# -*- coding: utf-8 -*-

from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options

class FirefoxWebdriver(Firefox):
    def __init__(self):
        options = Options()
        options.add_argument("--disable-infobars")
        options.add_argument("--headless")
        
        driverpath="/usr/bin/geckodriver"

        super().__init__(executable_path=driverpath, firefox_options=options)

if __name__ == "__main__":
    a = FirefoxWebdriver()

    

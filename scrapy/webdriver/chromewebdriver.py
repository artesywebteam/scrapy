#!/usr/bin/python3
# -*- coding: utf-8 -*-

from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options

class ChromeWebdriver(Chrome):
    def __init__(self):
        options = Options()
        options.add_argument("--disable-infobars")
        options.add_argument("--headless")
        
        driverpath="/usr/bin/chromedriver"

        super().__init__(driverpath, chrome_options=options)


if __name__ == "__main__":
    a = ChromeWebdriver()

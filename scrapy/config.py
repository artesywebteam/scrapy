#!/usr/bin/python3
# -*- coding: utf-8 -*-

import configparser
from pathlib import Path

class Config:
    
    def __init__(self):
        """docstring for Config."""
        
        env_path = Path().resolve().joinpath('.env')

        self.config = configparser.ConfigParser()
        with open(str(env_path)) as ini_file:
            self.config.readfp(ini_file)
#!/usr/bin/python3
# -*- coding: utf-8 -*-

import requests
from requests.exceptions import Timeout,ConnectTimeout,ConnectionError

from .db import DB

class Session:
    def __init__(self):
        # database connection
        self.db = DB.getInstance()

        # get ip from session
        self.current_ip = self.getCurrentIP()

        # set current id session
        self.session_id = None
    
    def getCurrentIP(self):
        """Return current session IP"""
        try:
            response = requests.get('https://api.ipify.org', timeout=5)
            if not response.raise_for_status():
                return response.text

        except (Timeout,ConnectionError, ConnectTimeout):
            self.current_ip = None
            self.registerSession("",0,False)
            exit()
        
        return None

    def registerSession(self, query, ads_count=0, internet=True, error=False):
        """Register in database current session """
        cursor = self.db.cursor()
        cursor.execute("""
            insert into session (datetime, query, ads_count, ip, internet, error)
            values (date('now'),?,?,?,?,?)
            """ ,(query, ads_count, self.current_ip, internet, error)
            )
        self.db.commit()
        self.session_id = cursor.lastrowid
        cursor.close()
    
    def registerSessionAds(self, page, ad_url):
        """
        Register in database all recolected ads 
        in current session
        """
        """ 
        check if the page and ad url are equal
        if not the click is not success
        """
        click_success = True if page == ad_url else False

        cursor = self.db.cursor()
        cursor.execute("""
            insert into session_ads(id_session, page, ad_url, click_success)
            values (?,?,?,?)
        """, (self.session_id, page, ad_url, click_success))
        self.db.commit()
        cursor.close()
    
    def getRandomGoogleUser(self):
        self.db.cursor()
        cursor.execute("""
            select * from google_users order by random() limit 1
        """)

    # TODO: if historic session with ip not showing ads then
    # do something else (reboot or reboot the intelligent switch)
    def checkIfGoogleStopShowingAds(self):
        """Check if Google Ads are not showing"""
        cursor = self.db.cursor()
        cursor.execute("""
            select count(id)
            from session
            where internet = 1
            and ads_count = 0
            and ip = '?'
        """, (self.current_ip))
#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sqlite3

class DB:
    con = None

    def connect():
        try:
            DB.con = sqlite3.connect('scraps.db')
        except sqlite3.Error as e:
            pass
        

    def getInstance():
        if not DB.con:
            DB.connect()
            DB.createTables()
            
        return DB.con
    
    def createTables():
        con = DB.getInstance()

        try:
            con.execute("PRAGMA foreign_keys = ON")
        except sqlite3.Error as sqle:
            pass

        try:
            con.executescript("""
                CREATE TABLE IF NOT EXISTS session(
                    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    datetime TIMESTAMP NOT NULL,
                    query TEXT NOT NULL,
                    ads_count UNSIGNED TINYINTEGER DEFAULT 0,
                    ip TEXT NULL,
                    internet BOOLEAN DEFAULT 1,
                    error BOOLEAN DEFAULT 0
                );
            """)
        except sqlite3.Error as e:
            pass

        try:
            con.executescript("""
                CREATE TABLE IF NOT EXISTS session_ads(
                    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    id_session REFERENCES session(id),
                    page TEXT NOT NULL,
                    ad_url TEXT NOT NULL,
                    click_success BOOLEAN NULL
                );
            """)
        except sqlite3.Error as e:
            pass

        try:
            con.executescript("""
                CREATE TABLE IF NOT EXISTS google_users(
                    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    id_session REFERENCES session(id),
                    page TEXT NOT NULL,
                    ad_url TEXT NOT NULL,
                    click_success BOOLEAN NULL
                );
            """)
        except sqlite3.Error as e:
            pass
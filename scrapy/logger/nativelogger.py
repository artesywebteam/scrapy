#!/usr/bin/python3
# -*- coding: utf-8 -*-

import logging

class Logger(object):
    """docstring for NativeLogger."""
    def __init__(self):
        self.log = logging
        self.log.basicConfig(filename="scrapy.log", level=self.log.INFO, format='%(asctime)s : %(levelname)s : %(message)s')

    def info(self, message):
        self.log.info(message)

    def debug(self, message):
        self.log.debug(message)

    def error(self, message):
        self.log.error(message)

    def warning(self, message):
        self.log.warning(message)
